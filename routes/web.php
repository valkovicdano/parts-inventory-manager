<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return redirect()->route('cars.index');
});

Route::resource('cars', \App\Http\Controllers\CarController::class);
Route::resource('parts', \App\Http\Controllers\PartController::class);
